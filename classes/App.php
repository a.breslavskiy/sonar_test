<?php

class App
{
    private static $instance;

    public static function getDbConnection(): \PDO
    {
        $dsn = 'this_is_just_for_test';
        return new \PDO($dsn);
    }

    public static function i()
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        return self::$instance;
    }

}