<?php

namespace VendorClassesUsed;

use SonarTestVendor\Request;
use SonarTestVendor\App;

/**
 * @author DevOps <devops@b2b-center.ru>
 */
class Model
{
    public function checkSql() {
        $search_text = Request::getString('search_text', $_REQUEST);
        $q = "SELECT * FROM b2bcenter.test_table WHERE text LIKE '$search_text%'";
        $db = App::getDbConnection();
        $res = $db->query($q);
        return $res->fetchAll();
    }

    public function checkSql2() {
        $search_text = $_REQUEST['search_text'];
        $q = "SELECT * FROM b2bcenter.test_table WHERE text LIKE '$search_text%'";
        $db = App::getDbConnection();
        $res = $db->query($q);
        return $res->fetchAll();
    }

}