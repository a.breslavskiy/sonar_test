<?php

namespace Base;

class StaticRequest
{
    public static function getString($variable_name, array $source_array, $default_value = false)
    {
        if (
            !isset($source_array[$variable_name])
            || !is_scalar($source_array[$variable_name])
        ) {
            return $default_value;
        }
        return (string)$source_array[$variable_name];
    }


}