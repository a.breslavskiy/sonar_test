<?php

namespace Base;

class DynamicRequest
{
    private static $instance;

    public function getString($variable_name, array $source_array, $default_value = false)
    {
        if (
            !isset($source_array[$variable_name])
            || !is_scalar($source_array[$variable_name])
        ) {
            return $default_value;
        }
        return (string)$source_array[$variable_name];
    }

    public static function i()
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }
        return self::$instance;
    }



}