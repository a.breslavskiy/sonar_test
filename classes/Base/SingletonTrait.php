<?php

namespace Base;

trait SingletonTrait
{

    /** @var static */
    protected static $instance;

    /**
     * @param array $params
     * @return static
     */
    public static function i(...$params)
    {
        return static::$instance ?: static::$instance = new static(...$params);
    }

    /**
     * Конструктор
     * @param array $params
     */
    final private function __construct(...$params)
    {
        $this->init(...$params);
    }

    /**
     * Инициализация
     * @param array $args
     */
    protected function init(...$args)
    {
    }

    final private function __clone()
    {
    }
}
