<?php

namespace SingletonUsed;

use RequestSingletonChild as Request;
use Base\DynamicRequest as RequestParent;
use App;

/**
 * @author DevOps <devops@b2b-center.ru>
 */
class Test
{
    public function checkSql() {
        $search_text = Request::i()->getString('search_text', $_REQUEST);
        $q = "SELECT * FROM b2bcenter.test_table WHERE text LIKE '$search_text%'";
        $db = App::getDbConnection();
        $res = $db->query($q);
        return $res->fetchAll();
    }

    public function checkSql2() {
        $search_text = $_REQUEST['search_text'];
        $q = "SELECT * FROM b2bcenter.test_table WHERE text LIKE '$search_text%'";
        $db = App::getDbConnection();
        $res = $db->query($q);
        return $res->fetchAll();
    }

    public function checkSql3() {
        $search_text = RequestParent::i()->getString('search_text', $_REQUEST);
        $q = "SELECT * FROM b2bcenter.test_table WHERE text LIKE '$search_text%'";
        $db = App::getDbConnection();
        $res = $db->query($q);
        return $res->fetchAll();
    }

    public function checkSql4() {
        $search_text = $_REQUEST['search_text'];
        $q = "SELECT * FROM b2bcenter.test_table WHERE text LIKE '$search_text%'";
        $db = App::i()->getDbConnection();
        $res = $db->query($q);
        return $res->fetchAll();
    }

}